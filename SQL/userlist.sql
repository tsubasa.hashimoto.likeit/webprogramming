create table user (id serial not null primary key,
 login_id varchar(255) unique not null,
 name varchar(255) not null,
 birth_date date not null,
 password varchar(255) not null,
 create_date datetime not null,
 update_date datetime not null);