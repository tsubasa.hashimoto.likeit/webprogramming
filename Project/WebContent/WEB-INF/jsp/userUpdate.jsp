<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import = "model.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link href = "css/Style.css" rel = "stylesheet" type = "text/css"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<header>
<ul>
	<li>${userInfo.name}さん　　</li>
	<li><a href = "LogoutServlet">ログアウト</a></li>
</ul>
</header>
<h1>ユーザ情報更新</h1>

<c:if test="${err != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${err}
		</div>
	</c:if>

<form action = "UpdateServlet" method = "post">
<input type = "hidden" name = "id" value = "${user.id}">
<input type = "hidden" name = "loginId" value = "${user.loginId}">
	<table class = "user_cf">
		<tr>
         	<th>ログインID</th>
            <th>${user.loginId}</th>
   		</tr>
   		<tr>
   			<th>パスワード</th>
   			<th><input type = "password" name = "password"></th>
   		</tr>
   		<tr>
   			<th>パスワード(確認)</th>
   			<th><input type = "password" name = "passwordConfirm"></th>
   		</tr>
   		<tr>
   			<th>ユーザー名</th>
   			<th><input type = "text" name = "name" value = "${user.name}"></th>
   		</tr>
   		<tr>
   			<th>生年月日</th>
   			<th><input type = "date" name = "birthDate" value = "${user.birthDate}"></th>
   		</tr>
	</table>
	<p class = "updatebtn"><button type="submit" class="btn btn-primary">更新</button></p>
</form>

<a href = "UserListServlet">戻る</a>
</body>
</html>