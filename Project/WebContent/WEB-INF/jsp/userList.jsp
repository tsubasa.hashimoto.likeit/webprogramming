<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="model.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	User user = (User) request.getAttribute("userInfo");
%>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link href="css/Style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

<header>
		<ul>
			<li class="user_name">${userInfo.name}さん</li>
			<li class="logout"><a href="LogoutServlet" class="logout">ログアウト</a></li>
		</ul>


	</header>
	<h1>ユーザー一覧</h1>

	<p class="user_signup">
		<a href="CreateServlet">新規登録</a>
	</p>

	<form action="UserListServlet" method="post">
		<p class="user_form">
			ログインID <input type="text" name="loginId">
		</p>

		<p class="user_form">
			名前 <input type="text" name="name">
		</p>
		<p class="user_form">
			生年月日 <input type="date" name="dateStart"> ～ <input
				type="date" name="dateEnd">
		</p>
		<p class = "updatebtn">
			<button type="submit" class="btn btn-primary">検索</button>
		</p>
	</form>

<table>
	<tr>
	<th>ID</th>
	<th>名前</th>
	<th>生年月日</th>
	</tr>
	<c:forEach var = "user" items = "${list}">
	<tr>
	<td>${user.loginId}</td>
	<td>${user.name}</td>
	<td>${user.birthDate}</td>

	<td><a class="color blue button" href="UserDetailServlet?id=${user.id}">詳細</a>
	 						<c:if test="${userInfo.loginId == user.loginId or userInfo.loginId == 'admin'}">
							<a class="color green button" href="UpdateServlet?id=${user.id}">更新</a>
	 						</c:if>
	 						<c:if test="${userInfo.loginId == 'admin'}">
	 						<a class="color red button" href="DeleteServlet?id=${user.id}">削除</a>
	 						</c:if>
						</td>
	</tr>
	</c:forEach>
	</table>


</body>
</html>