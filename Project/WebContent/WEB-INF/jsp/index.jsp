<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ログイン画面</title>
    <link href = "css/Style.css" rel = "stylesheet" type = "text/css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>

    <h1>ログイン画面</h1>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


    <form action = "LoginServlet" method = "post">
   <p class = "login">ログインID　　　　　　
   <input type = "text" name = "loginId"></p>
  	<p class = "login">パスワード　　　　　　
  	<input type = "password" name = "password" required></p>
  	<p class = "login_butt">
  	<button type="submit" class="btn btn-primary">ログイン</button>
  	</p>
	</form>

  </body>
</html>
