<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
<link href="css/Style.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧画面</title>

</head>
<body>
	<header>


		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>

	</header>


	<h1>ユーザ情報詳細参照</h1>
	<input type = "hidden" name = "id">
	<table class="user_cf">

		<tr>
			<th>ログインID</th>
			<th>${user.loginId}</th>
		</tr>
		<tr>
			<th>ユーザ名</th>
			<th>${user.name}</th>
		</tr>
		<tr>
			<th>生年月日</th>
			<th>${user.birthDate}</th>
		</tr>
		<tr>
			<th>登録日時</th>
			<th>${user.createDate}</th>
		</tr>
		<tr>
			<th>更新日時</th>
			<th>${user.updateDate}</th>
		</tr>

	</table>

	<p class="back">
		<a href="UserListServlet">戻る</a>
	</p>
</body>
</html>