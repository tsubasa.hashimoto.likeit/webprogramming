<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<title>ユーザ新規登録</title>
<link href = "css/Style.css" rel = "stylesheet" type = "text/css"/>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<header>
<ul>
	<li class = "user_name">${userInfo.name}さん　　　　</li>
	<li class = "logout"><a href = "LogoutServlet" class = "logout">ログアウト</a></li>
</ul>
</header>

<h1>ユーザ新規登録</h1>
<c:if test="${msg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${msg}
		</div>
	</c:if>
	<form action = "CreateServlet" method = "post">
	<table class = "user_cf">

		<tr>
			<th>ログインID</th>
			<th><input type = "text" name = "loginId"></th>
		</tr>
		<tr>
			<th>パスワード</th>
			<th><input type = "password" name = "password"></th>
		</tr>
		<tr>
			<th>パスワード（確認）</th>
			<th><input type = "password" name = "passwordConfirm"></th>
		</tr>
		<tr>
			<th>ユーザー名</th>
			<th><input type = "text" name = "name"></th>
		</tr>
		<tr>
			<th>生年月日</th>
			<th><input type = "date" name = "birthDate"></th>
		</tr>

	</table>
	<p class = "Regist">
	<button type="submit" class="btn btn-primary">登録</button>
	</p>
	</form>

	<p class = "back"><a href = "UserListServlet">戻る</a></p>
</body>
</html>