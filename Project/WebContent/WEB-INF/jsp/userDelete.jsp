<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import = "model.*"%>
<% User user = (User)request.getAttribute("userInfo");%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除確認</title>
<link href="css/Style.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<header>
		<ul>
			<li class="user_name">${userInfo.name}さん</li>　　　
			<li class="logout"><a href= "LogoutServlet" class = "logout">ログアウト</a></li>

		</ul>
	</header>

	<h1>ユーザ削除確認</h1>

	<p>ログインID：${user.loginId}</p>
	<p>を本当に削除してよろしいでしょうか。</p>

	<div class="login_butt">
		<p><a href = "UserListServlet"><button type="submit" class="btn btn-primary">キャンセル</button></a>　　　　
		<form action = "DeleteServlet" method = "post">
		<input type = "hidden" name = "id" value = "${user.id}">
		<button type="submit" class="btn btn-primary">OK</button></p>
		</form>
	</div>
</body>
</html>