package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userInfo");

		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
			return;
		} else {

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取

		int id = 0;
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String createDate = sdf.format(date);
		String updateDate = sdf.format(date);
		System.out.println(loginId);

		UserDao dao = new UserDao();
		User user = new User();
		List<User> list = dao.findloginId(loginId);

		// パスワードとパスワード(確認)の入力内容が異なる場合
		// 入力項目に１つでも未入力がある場合
		// 登録に失敗した場合入力フォームに戻る
		if (!password.equals(passwordConfirm) || loginId.isEmpty() || name.isEmpty() || birthDate.isEmpty() ||
				password.isEmpty() || passwordConfirm.isEmpty() || list.contains(loginId)) {

			request.setAttribute("msg", "入力された内容が正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
		}else {

		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		try {
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			String pass = DatatypeConverter.printHexBinary(bytes);
			user = new User(id, loginId, name, birthDate, pass, createDate, updateDate);
			dao.create(user);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		response.sendRedirect("UserListServlet");

		}

	}

}
