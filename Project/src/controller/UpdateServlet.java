package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		HttpSession session = request.getSession();

		User user = null;
		user = (User)session.getAttribute("userInfo");

		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
			return;
		} else {

		String id = request.getParameter("id");
		System.out.println(id);
		UserDao dao = new UserDao();
		user = dao.findOne(Integer.parseInt(id));
		System.out.println(user.getLoginId());
		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		request.setCharacterEncoding("UTF-8");
		//フォームから取得
		String id = request.getParameter("id");
		int num = Integer.parseInt(id);
		System.out.println(num);

		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		User user;
		System.out.println(password + ":" + passwordConfirm + ":" + name + ":" + birthDate);

		// パスワードとパスワード(確認)が異なる場合
		// パスワード以外に未入力がある場合
		// 入力画面に戻る
		if (!password.equals(passwordConfirm) || name.isEmpty() || birthDate.isEmpty()) {
			System.out.println("入力された内容は正しくありません");
			request.setAttribute("err", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

		}
		// パスワードとパスワード(確認)がどちらも空欄の場合
		// パスワードは更新せずにパスワード以外更新
		else if(password.isEmpty() && passwordConfirm.isEmpty() && !name.isEmpty() && !birthDate.isEmpty()){
	    System.out.println("パスワードとパスワード(確認)がどちらも空欄の場合");
	    System.out.println(num + ":" + name + ":" + birthDate);

		user = new User(num, name, birthDate);
		UserDao dao = new UserDao();
		dao.updateNameBirth(user);

		response.sendRedirect("UserListServlet");

	    }else{

		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		try {
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			String pass = DatatypeConverter.printHexBinary(bytes);
			user = new User(num, name, birthDate, pass);
			UserDao dao = new UserDao();
			dao.update(user);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		response.sendRedirect("UserListServlet");
	}
}}
