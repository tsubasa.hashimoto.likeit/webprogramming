package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public User create(User user) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user VALUES(?,?,?,?,?,?,?)";

			stmt = conn.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			int isId = 0;
			stmt.setInt(1, isId);
			stmt.setString(2, user.getLoginId());
			stmt.setString(3, user.getName());
			stmt.setString(4, user.getBirthDate());
			stmt.setString(5, user.getPassword());
			stmt.setString(6, user.getCreateDate());
			stmt.setString(7, user.getUpdateDate());

			// 登録SQLを実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public List<User> serch(User user) {

		Connection con = null;
		List<User> list = new ArrayList<User>();

		try {
			con = DBManager.getConnection();

			String sql = "SELECT*FROM user where 1=1";
			ArrayList<String> flag = new ArrayList<String>();
			System.out.println(user.getLoginId() + user.getName() + user.getDateStart() + user.getDateEnd());

			if (!user.getLoginId().isEmpty()) {
				sql += " and login_id = ?";
				flag.add("loginId");
			}

			if (!user.getName().isEmpty()) {
				sql += " and name like ?";
				flag.add("name");
			}

			if (!user.getDateStart().isEmpty()) {
				sql += " and birth_date between ?";
				flag.add("dateStart");
			}
			if (!user.getDateEnd().isEmpty()) {
				sql += " and ?";
				flag.add("dateEnd");
			}

			PreparedStatement ps = con.prepareStatement(sql);
			int index = 0;
			if (flag.contains("loginId")) {
				ps.setString(++index, user.getLoginId());
			}

			if (flag.contains("name")) {
				ps.setString(++index, "%" + user.getName() + "%");
			}

			if (flag.contains("dateStart")) {
				ps.setString(++index, user.getDateStart());
			}

			if (flag.contains("dateEnd")) {
				ps.setString(++index, user.getDateEnd());
			}
			System.out.println(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String createDate = rs.getString("create_date");
				System.out.println(id + ":" +loginId + ":" + name + ":" + birthDate + ":" + createDate);
				user = new User(id, loginId, name, birthDate, createDate);

				list.add(user);

			}

		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベースに切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return list;
	}

	public User detail(int id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		User user = new User();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT login_id, name, birth_date, create_date, update_date FROM user where id = ?";

			stmt = conn.prepareStatement(sql);

			stmt.setInt(1, id);
			stmt.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return user;
	}

	public void update(User user) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("update user set password = ?, name = ?, birth_date = ? where id = ?");

			ps.setString(1, user.getPassword());
			ps.setString(2, user.getName());
			ps.setString(3, user.getBirthDate());
			ps.setInt(4, user.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}

	public void updateNameBirth(User user) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DBManager.getConnection();
			ps = con.prepareStatement("update user set name = ?, birth_date = ? where id = ?");
			System.out.println(user.getId() + ":" + user.getName() + ":" + user.getBirthDate());
			ps.setString(1, user.getName());
			ps.setString(2, user.getBirthDate());
			ps.setInt(3, user.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}

	public void delete(int id) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE id = ?";
			// ステートメント生成
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User findOne(int id) {
		Connection con = null;
		User user = null;

		try {
			con = DBManager.getConnection();
			String sql = "select*from user where id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;

	}

	public List<User> findloginId(String loginId) {
		Connection conn = null;
		List<User> list = new ArrayList<>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT login_id FROM user where login_id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, loginId);
			ResultSet rs = ps.executeQuery();


			while (rs.next()) {
				String loginIdData = rs.getString("login_id");
				User user = new User(loginIdData);
				list.add(user);
				return list;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return list;
	}

	public User find(User user) {
		Connection con = null;
		int id = 0;

		try {
			con = DBManager.getConnection();
			String sql = "select*from user where login_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getLoginId());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;

	}
}
